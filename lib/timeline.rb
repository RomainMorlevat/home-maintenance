require "date"

class Timeline
  def initialize(events:, reminders:)
    @events = events
    @reminders = reminders
  end

  def all
    [all_events, all_reminders].flatten.compact.sort_by { |el| el[:date] || el[:due_date] }
  end

  def all_events
    @events.all
  end

  def all_reminders
    @reminders.all
  end

  def due_reminders
    @reminders.all.select { |reminder| Date.parse(reminder[:due_date]) <= Date.today }
  end
end
