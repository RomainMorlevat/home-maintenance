class Reminders
  def initialize
    @reminders = []
  end

  def add(due_date:, name:)
    return if due_date.nil? || name.nil?

    @reminders << {due_date: due_date, done: false, name: name}
  end

  def all
    @reminders
  end
end
