# Home maintenance

An application to keep track of home maintenance tasks, past and future.

## Technical goal

Explore app creation without a framework at first to only think about business logic.

## Technical specs

*   Ruby version: 3.1.2

### How to run the test suite
`bundle exec rspec`
