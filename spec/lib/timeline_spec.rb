require "date"
require "events"
require "reminders"
require "timeline"

RSpec.describe Timeline do
  subject(:timeline) { described_class.new(events: events, reminders: reminders) }

  let(:events) { Events.new }
  let(:reminders) { Reminders.new }

  describe "#all" do
    subject(:all) { timeline.all }

    context "when no events or reminders" do
      it { is_expected.to eq([]) }
    end

    context "when events" do
      before do
        events.add(date: "2022-11-26", name: "Chimney sweeping")
        reminders.add(due_date: "2022-11-25", name: "Reverse phyto-purification")
        reminders.add(due_date: Date.today.to_s, name: "Reverse phyto-purification")
      end

      it "returns events in a list" do
        expect(all).to eq([
          {due_date: "2022-11-25", done: false, name: "Reverse phyto-purification"},
          {date: "2022-11-26", name: "Chimney sweeping"},
          {due_date: Date.today.to_s, done: false, name: "Reverse phyto-purification"}])
      end
    end
  end

  describe "#all_events" do
    subject(:all_events) { timeline.all_events }

    context "when no events" do
      it "returns an empty list" do
        expect(all_events).to eq([])
      end
    end

    context "when events" do
      before { events.add(date: "2022-11-26", name: "Chimney sweeping") }

      it "returns events in a list" do
        expect(all_events).to eq([{date: "2022-11-26", name: "Chimney sweeping"}])
      end
    end
  end

  describe "#all_reminders" do
    subject(:all_reminders) { timeline.all_reminders }

    context "when no future reminders" do
      it { is_expected.to eq([]) }
    end

    context "when past reminders" do
      before { reminders.add(due_date: "2022-11-25", name: "Chimney sweeping") }

      it { is_expected.to eq([{due_date: "2022-11-25", done: false, name: "Chimney sweeping"}]) }
    end

    context "when future reminders" do
      before { reminders.add(due_date: Date.today.to_s, name: "Chimney sweeping") }

      it { is_expected.to eq([{due_date: Date.today.to_s, done: false, name: "Chimney sweeping"}]) }
    end

    context "when past and future reminders" do
      before do
        reminders.add(due_date: "2022-11-25", name: "Chimney sweeping")
        reminders.add(due_date: Date.today.to_s, name: "Reverse phyto-purification")
      end

      it { is_expected.to eq([
        {due_date: "2022-11-25", done: false, name: "Chimney sweeping"},
        {due_date: Date.today.to_s, done: false, name: "Reverse phyto-purification"}]) }
    end
  end

  describe "#due_reminders" do
    subject(:due_reminders) { timeline.due_reminders}

    context "when no due reminders" do
      it { is_expected.to eq([]) }
    end

    context "when only future reminders" do
      before { reminders.add(due_date: Date.today.next_day.to_s, name: "Chimney sweeping") }

      it { is_expected.to eq([]) }
    end

    context "when due reminders" do
      before { reminders.add(due_date: Date.today.to_s, name: "Chimney sweeping") }

      it { is_expected.to eq([{due_date: Date.today.to_s, done: false, name: "Chimney sweeping"}]) }
    end
  end
end
