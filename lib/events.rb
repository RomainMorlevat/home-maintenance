require "date"

class Events
  class NoFutureEvents < StandardError; end

  def initialize
    @events = []
  end

  def add(date:, name:)
    return if date.nil? || name.nil?
    raise NoFutureEvents if date > Date.today.to_s

    @events << {date: date, name: name}
  end

  def all
    @events
  end
end
