require "events"

RSpec.describe Events do
  subject(:events) { described_class.new }

  context "when adding an event" do
    before { events.add(date: "2022-11-26", name: "Chimney sweeping") }

    it "add it to events" do
      expect(events.all).to eq([{date: "2022-11-26", name: "Chimney sweeping"}])
    end
  end

  context "when the event do not have a name" do
    before { events.add(date: "2022-11-26", name: nil) }

    it "does not add it to events" do
      expect(events.all).to eq([])
    end
  end

  context "when the event do not have a date" do
    before { events.add(date: nil, name: "Chimney sweeping") }

    it "does not add it to events" do
      expect(events.all).to eq([])
    end
  end

  context "when adding an event with a future date" do
    it "raises an error" do
      expect { events.add(date: Date.today.next_day.to_s, name: "Chimney sweeping") }.to raise_error(Events::NoFutureEvents)
    end
  end
end
