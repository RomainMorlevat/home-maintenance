require "reminders"

RSpec.describe Reminders do
  subject(:reminders) { described_class.new }

  context "when adding an reminder" do
    before { reminders.add(due_date: "2022-11-26", name: "Chimney sweeping") }

    it "add it to reminders" do
      expect(reminders.all).to eq([{due_date: "2022-11-26", done: false, name: "Chimney sweeping"}])
    end
  end

  context "when the reminder do not have a name" do
    before { reminders.add(due_date: "2022-11-26", name: nil) }

    it "does not add it to reminders" do
      expect(reminders.all).to eq([])
    end
  end

  context "when the reminder do not have a due_date" do
    before { reminders.add(due_date: nil, name: "Chimney sweeping") }

    it "does not add it to reminders" do
      expect(reminders.all).to eq([])
    end
  end
end
